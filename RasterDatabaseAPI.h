#pragma once
#include <memory>
#include <vector>
#include <string>
#include <chrono>
#include "MaskInterface.h"
#include "SimpleTypes/Types.h"
#include "ERInterface/ERInterface.h"

namespace RasterDatabaseAPI {
	using namespace std;
	using namespace SimpleType;

	namespace Service {
		static char* getDatabaseInterfaceName() {
			return "RasterDB::getDBObject()";
		}
		static string version() {
			return "0.1";
		}
	}

	namespace RasterAccess {

		class ImageAttributes {
		public:
			virtual ~ImageAttributes() {}
			virtual const bool setBase(const string& frameName, const string& dataSourceName) = 0;
			virtual const bool set(const chrono::system_clock::time_point& acquisitionDate) = 0;
			virtual const bool set(const double cloudCover) = 0;

			virtual const bool isValid() const = 0;
		};

		class Database {
		public:
			enum FillMode { PARTIAL, FULL };
		public:
			virtual ~Database() {}
			//************************************
			// Method:    initialize
			// ��������� ������������� ���� ������
			//************************************
			virtual void initialize() = 0;

			//************************************
			// Method:    deinitialize
			// ��������� ������������ �������� ���� ������
			//************************************
			virtual void deinitialize() = 0;

			virtual const bool isInitialized() const = 0;

			//************************************
			// Method:    setFillMode
			// ���������� ���������� � ������������ ������.
			// FillMode::PARTIAL (�� ���������) ��������� ������� �� ��������� ������������ ������
			// FillMode::FULL ��������� ������� �� ��������� ������������ ������
			//************************************
			virtual void setFillMode(FillMode mode) = 0;

			virtual HIMAGE getRegion(const PointD& mid, const double resolution, const uint countOfTiles) = 0;
			virtual HIMAGE getRegion(const PointD& tl, const PointD& rb, const double resolution) = 0;
			virtual HIMAGE getRegion(const PointD& tl, const PointD& rb, const uint countOfTiles) = 0;

			virtual HIMAGE getLevel(const PointD& tl, const PointD& rb, const uint level) = 0;

			virtual HIMAGE toMercatorProjection(const HIMAGE frame, RectI sourceRect, const uint level) const = 0;

			virtual const uint levelFromResolution(const double resolution, const double latitude) const = 0;

			virtual const double getResolutionOfLevel(const double latitude, const uint level) const = 0;

			virtual const double getHeight(const PointD geoPoint) const = 0;

			virtual shared_ptr<ImageAttributes> makeAttributesObject() const = 0;
			virtual const bool addImage(const HIMAGE frame, const shared_ptr<ImageAttributes> imageAttributes) = 0;
		};
	}

	namespace TilesAccess {

		class TileRaster {
		public:
			virtual ~TileRaster() {};

			virtual unsigned char* scanLine(const int lineIndex) = 0;

			virtual const unsigned char* scanLine(const int lineIndex) const = 0;

			virtual const int width() const = 0;

			virtual const int height() const = 0;
		};

		struct Tile {
			shared_ptr<TileRaster> raster;
			shared_ptr<MaskInterface> mask;
		};

		class SceneMap {
		public:
			struct TilePosition {
				PointI positionOnScene;
				PointI tileIndex;
			};
		public:
			virtual ~SceneMap() {};
			virtual void setLevel(const int level) = 0;
			virtual void setSize(const SizeI& scenePixelsSize) = 0;
			virtual void setMidPoint(const PointD& sceneMidGeoPoint) = 0;
			virtual const vector<TilePosition> sceneMap() const = 0;
		};

		class Level {
			virtual ~Level() {};
			virtual const int scaleIndex() const = 0;
			virtual const Tile tile(const PointI& position) const = 0;
		};

		class Layer {
		public:
			virtual ~Layer() {};
			virtual vector<int> levels() const = 0;
			virtual shared_ptr<Level> level(const int scaleIndex) const = 0;
		};

		class TilesDatabase {
		public:
			enum class UnitsOfMeasurement : int {
				PIXELS = 0,
				TILES = 1
			};
		public:
			virtual ~TilesDatabase() {};
			virtual const vector<string> dataSets() const = 0;
			virtual const SimpleType::SizeI sizeOfLevel(const int level, const UnitsOfMeasurement unit = UnitsOfMeasurement::PIXELS) const = 0;
			virtual shared_ptr<SceneMap> makeSceneMap() const = 0;
			virtual const shared_ptr<Layer> layer(const string& datasetName) const = 0;
		};
	}


	class Interface {
	public:
		virtual ~Interface() {}
		virtual shared_ptr<RasterAccess::Database> rasterAccessObject() const = 0;
		virtual shared_ptr<TilesAccess::TilesDatabase> tilesAccessObject() const = 0;
		virtual shared_ptr<string> version() const = 0;
	};


	// � ������ ������ ��� ��������� ���������� ����� ��������� ���������� std::logic_error
	static shared_ptr<Interface> getDatabaseObject() {
		typedef shared_ptr<Interface>(*GetDatabaseObjectFn)();

		GetDatabaseObjectFn getDatabaseObjectFn = static_cast<GetDatabaseObjectFn>(GetNamedAddress(Service::getDatabaseInterfaceName()));

		if (getDatabaseObjectFn == nullptr) {
			throw std::logic_error("�� ������ ��������� Database. ��������� ������� RasterDB.dll");
		}
		shared_ptr<Interface> interfaceObject = getDatabaseObjectFn();

		if (!interfaceObject) {
			throw logic_error("�� ������� �������� ��������� Database");
		}
		const auto dllVersion = interfaceObject->version();

		if (dllVersion && *dllVersion != Service::version()) {
			throw logic_error("����������������� ������ ���������� RasterDatabaseAPI. ��������� ������: " +
				*interfaceObject->version());
		}
		return interfaceObject;
	}
}
