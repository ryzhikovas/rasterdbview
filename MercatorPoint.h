#pragma once

template <class T>
class MercatorPoint {
private:
	NativePoint<T> p;

	unsigned long worldWidth;
	double bigSemiaxis;
	double smallSemiaxis;
public:
	inline MercatorPoint(const unsigned long worldWidth = 256);
	inline MercatorPoint(const T x, const T y, const unsigned long worldWidth = 256);
	inline MercatorPoint(const GeoPt& geo, const unsigned long worldWidth = 256);

	inline const GeoPt geo() const;

	inline const T x() const;
	inline const T y() const;
	inline const T resolution() const;

	inline MercatorPoint& scaleUp(const unsigned multipler);
	inline MercatorPoint& scaleDown(const unsigned divider);

	inline MercatorPoint& set(const T x, const T y);
	inline MercatorPoint& set(const T x, const T y, const unsigned long worldWidth);
	inline MercatorPoint& setGeo(const GeoPt& geo);
	inline MercatorPoint& setX(const T x);
	inline MercatorPoint& setY(const T y);

	inline MercatorPoint& moveX(const T dx);
	inline MercatorPoint& moveY(const T dy);

	template <class T_>
	friend const T_ distance(const MercatorPoint<T_>& left, const MercatorPoint<T_>& right);
private:
	inline const T plusLongitude(T value, T adden);
	inline const T plusLatitude(T value, T adden);

	inline NativePoint<T> toMercator(const GeoPt& geo) const;

	inline static constexpr const double earthSphereRadius();
	inline static const double bigSemiaxisValue(const unsigned long worldWidth);
	inline static const double smallSemiaxisValue(const unsigned long worldWidth);
	inline static constexpr const double bigSemiaxisValue();
	inline static constexpr const double smallSemiaxisValue();
	inline static constexpr const double lengthOfTheEquator();
	inline static constexpr const double lengthOfTheMeredian();
};

using MercPt = MercatorPoint<double>;


template <class T>
constexpr const double MercatorPoint<T>::earthSphereRadius() {
	return 6371000.0;
}


template <class T>
const double MercatorPoint<T>::bigSemiaxisValue(const unsigned long worldWidth) {
	return static_cast<double>(worldWidth) / (2.0 * M_PI);
}


template <class T>
const double MercatorPoint<T>::smallSemiaxisValue(const unsigned long worldWidth) {
	auto big = bigSemiaxisValue(worldWidth);
	return big - (big / 298.257223563);
}


template <class T>
constexpr const double MercatorPoint<T>::bigSemiaxisValue() {
	return 6378137;
}


template <class T>
constexpr const double MercatorPoint<T>::smallSemiaxisValue() {
	return 6356752.314245179300000;
}


template <class T>
constexpr const double MercatorPoint<T>::lengthOfTheEquator() {
	return bigSemiaxisValue() * 2 * M_PI;
}


template <class T>
constexpr const double MercatorPoint<T>::lengthOfTheMeredian() {
	return smallSemiaxisValue() * 2 * M_PI;
}


template <class T>
NativePoint<T> MercatorPoint<T>::toMercator(const GeoPt& geoPoint) const {
	const PointD radianGP(geoPoint.x() * M_PI / 180.0, geoPoint.y() * M_PI / 180.0);
	NativePoint<T> result;
	result.x = bigSemiaxis * radianGP.x();

	const double f = (bigSemiaxis - smallSemiaxis) / bigSemiaxis;
	const double eccentricity = sqrt(2.0 * f - f * f);

	const double tangensPart = tan(M_PI_4 + radianGP.y() / 2.0);
	const double eSinLat = eccentricity * sin(radianGP.y());
	const double powPart = pow((1 - eSinLat) / (1.0 + eSinLat), eccentricity / 2.0);
	result.y = bigSemiaxis * log(tangensPart * powPart);

	return result;
}


template <class T>
MercatorPoint<T>::MercatorPoint(const unsigned long worldWidth):
	worldWidth(worldWidth),
	bigSemiaxis(bigSemiaxisValue(worldWidth)),
	smallSemiaxis(smallSemiaxisValue(worldWidth)),
	p(0, 0) {
}


template <class T>
MercatorPoint<T>::MercatorPoint(const T x, const T y, const unsigned long worldWidth):
	p(x, y),
	worldWidth(worldWidth),
	bigSemiaxis(bigSemiaxisValue(worldWidth)),
	smallSemiaxis(smallSemiaxisValue(worldWidth)) {
}


template <class T>
MercatorPoint<T>::MercatorPoint(const GeoPt& geo, const unsigned long worldWidth):
	worldWidth(worldWidth),
	bigSemiaxis(bigSemiaxisValue(worldWidth)),
	smallSemiaxis(smallSemiaxisValue(worldWidth)) {
	setGeo(geo);
}


template <class T>
const T MercatorPoint<T>::x() const {
	return p.x;
}


template <class T>
const T MercatorPoint<T>::y() const {
	return p.y;
}


template <class T>
const T MercatorPoint<T>::resolution() const {
	const double radianLatitude = geo().y() * M_PI / 180.0;
	return cos(radianLatitude) * lengthOfTheEquator() / worldWidth;
}


template <class T>
MercatorPoint<T>& MercatorPoint<T>::scaleUp(const unsigned multipler) {

	if (multipler == 0) {
		throw logic_error("������ ��� ����� �������� MercatorPoint: ��������� 0");
	}

	worldWidth *= static_cast<unsigned long>(multipler);
	p.x *= static_cast<T>(multipler);
	p.y *= static_cast<T>(multipler);
	return *this;
}


template <class T>
MercatorPoint<T>& MercatorPoint<T>::scaleDown(const unsigned divider) {

	if (divider == 0) {
		throw logic_error("������ ��� ����� �������� MercatorPoint: �������� 0");
	}

	worldWidth /= static_cast<unsigned long>(divider);
	p.x /= static_cast<T>(divider);
	p.y /= static_cast<T>(divider);
	return *this;
}


template <class T>
MercatorPoint<T>& MercatorPoint<T>::set(const T x, const T y) {
	return setX(x).setY(y);
}


template <class T>
MercatorPoint<T>& MercatorPoint<T>::set(const T x, const T y, const unsigned long worldWidth) {
	this->worldWidth = worldWidth;
	bigSemiaxis = bigSemiaxisValue(worldWidth);
	smallSemiaxis = smallSemiaxisValue(worldWidth);
	return set(x, y);
}


template <class T>
MercatorPoint<T>& MercatorPoint<T>::setGeo(const GeoPt& geo) {
	p = toMercator(geo);
	return *this;
}


template <class T>
MercatorPoint<T>& MercatorPoint<T>::setX(const T x) {
	p.x = x;
	return *this;
}


template <class T>
MercatorPoint<T>& MercatorPoint<T>::setY(const T y) {
	p.y = y;
	return *this;
}


template <class T>
MercatorPoint<T>& MercatorPoint<T>::moveX(const T dx) {
	setX(plusLongitude(x(), dx));
	return *this;
}


template <class T>
MercatorPoint<T>& MercatorPoint<T>::moveY(const T dy) {
	setY(plusLatitude(y(), dy));
	return *this;
}


template <class T>
const GeoPt MercatorPoint<T>::geo() const {
	const double f = (bigSemiaxis - smallSemiaxis) / bigSemiaxis;
	const double eccentricity = sqrt(2 * f - f * f);

	const double xValue = x() / bigSemiaxis;

	const double expPart = exp(-y() / bigSemiaxis);
	double atanPart = M_PI_2 - 2.0 * atan(expPart);

	int i(0);
	double dPhi(1.0);
	double con;

	while ((fabs(dPhi) > 0.000000001) && (i++ < 15)) {
		con = eccentricity * sin(atanPart);
		dPhi = M_PI_2 - 2 * atan(expPart * pow(((1 - con) / (1 + con)), eccentricity / 2)) - atanPart;
		atanPart += dPhi;
	}
	return GeoPt(xValue * 180.0 / M_PI, atanPart * 180.0 / M_PI);
}


template <class T>
const T MercatorPoint<T>::plusLongitude(T value, T adden) {
	const long border = static_cast<long>(worldWidth / 2);

	auto offseted = value + border;
	offseted += adden;

	T rest;
	T offsettedTrunc;
	rest = std::modf(offseted, &offsettedTrunc);

	long offsettedTruncInt = static_cast<long>(offsettedTrunc);
	offsettedTruncInt = offsettedTruncInt % (2 * border);

	offseted = offsettedTruncInt + rest;

	if (offseted <= 0) {
		offseted += static_cast<T>(border);
	}
	else {
		offseted -= static_cast<T>(border);
	}
	return offseted;
}


template <class T>
const T MercatorPoint<T>::plusLatitude(T value, T adden) {
	value += adden;

	if (abs(value) > 90.0) {
		value -= adden;
		throw logic_error("������� ����������� ������� 90� �� ������");
	}
	return value;
}


template <class T>
const T distance(const MercatorPoint<T>& left, const MercatorPoint<T>& right) {
	const auto lGeo = left.geo();
	const auto rGeo = right.geo();

	PointD a(lGeo.x() * M_PI / 180.0, lGeo.y() * M_PI / 180.0);
	PointD b(rGeo.x() * M_PI / 180.0, rGeo.y() * M_PI / 180.0);

	double val = sin(a.y()) * sin(b.y()) + cos(a.y()) * cos(b.y()) * cos(a.x() - b.x());

	//�������� ����� ������� ��������, ��� ���������� ����� ������� ����� ������ � ����. ������ ���������� ����������
	if (val > 1.0f) {
		val = 1.0f;
	}
	return MercatorPoint<T>::earthSphereRadius() * acos(val);
}