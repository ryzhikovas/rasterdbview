#pragma once
#include "stdafx.h"
#include "Widget.h"
#include "MapDataAccessor.h"

class Map;


class MapWidget: public Widget {
protected:
	shared_ptr<Map> sceneMap;
	shared_ptr<MapDataAccessor> accessor;
public:
	virtual ~MapWidget() override;
	virtual void update() const;
private:
	cocos2d::Sprite* fromData(const shared_ptr<MapData>& data) const;
};

