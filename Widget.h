#pragma once
#include "stdafx.h"

class Widget {
protected:
	cocos2d::Layer* layer;
public:
	Widget();
	Widget(cocos2d::Layer* layer);
	virtual ~Widget();
	virtual cocos2d::Layer* getLayer();
};

