#include "stdafx.h"
#include "MercatorMap.h"


MercatorMap::MercatorMap(const MercPt& screenMid, const SizeI& size):
	size(size),
	screenMid(screenMid) {
}


shared_ptr<MercatorMap> MercatorMap::build(const SizeI& screenSize, const MercPt& geoPos) {

	if (screenSize.width() <= 0 || screenSize.height() <= 0) {
		throw logic_error("������� �������� ������� ��������� �����");
	}
	return shared_ptr<MercatorMap>(new MercatorMap(geoPos, screenSize));
}


const MapFragment MercatorMap::currentFragment() const {
	const auto topLeft = topLeftScreen();
	const auto bottomRight = bottomRightScreen();

	const SizeD geoSize(
			distance(topLeft, MercPt(bottomRight.x(), topLeft.y())),
			distance(topLeft, MercPt(topLeft.x(), bottomRight.y())));

	return MapFragment::build(screenMid.geo(), geoSize, screenMid.resolution());
}


const MercPt MercatorMap::topLeftScreen() const {
	auto topLeft = screenMid;
	topLeft.moveX(-size.width() / 2).moveY(-size.height() / 2);
	return topLeft;
}


const MercPt MercatorMap::bottomRightScreen() const {
	auto bottomRight = screenMid;
	bottomRight.moveX(size.width() / 2).moveY(size.height() / 2);
	return bottomRight;
}


const SizeI MercatorMap::screenSize() const {
	return size;
}


void MercatorMap::resizeScreen(const SizeI& size) {
	this->size = size;
}


const GeoPt MercatorMap::screenToGeo(const PointD& screenPoint) const {
	auto mercatorPt = topLeftScreen();
	return mercatorPt.moveX(screenPoint.x()).moveY(screenPoint.y()).geo();
}


const PointD MercatorMap::geoToScreen(const GeoPt& geo) const {
	auto temp = screenMid;
	temp.setGeo(geo);
	auto mercatorPt = topLeftScreen();

	return PointD(temp.x() - mercatorPt.x(), temp.y() - mercatorPt.y());
}


void MercatorMap::toPoint(const GeoPt& point) {
	screenMid = point;
}


void MercatorMap::zoomUp(const unsigned modifer) {
	screenMid.scaleUp(modifer);
}


void MercatorMap::zoomDown(const unsigned modifer) {
	screenMid.scaleDown(modifer);
}
