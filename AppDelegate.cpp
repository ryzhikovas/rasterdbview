#include "stdafx.h"
#include "AppDelegate.h"
#include "Screen.h"
#include "ScreenModel.h"

USING_NS_CC;
using namespace std;

AppDelegate::AppDelegate() {
}

AppDelegate::~AppDelegate() {
}

void AppDelegate::initGLContextAttrs() {
	GLContextAttrs glContextAttrs = { 8, 8, 8, 8, 24, 8 };
	GLView::setGLContextAttrs(glContextAttrs);
}

bool AppDelegate::applicationDidFinishLaunching() {
	auto director = Director::getInstance();

	auto glview = director->getOpenGLView();

	if (!glview) {
		glview = GLViewImpl::create("Cpp Empty Test");
		director->setOpenGLView(glview);
	}
	// Set the design resolution
	glview->setFrameSize(1024, 768);
	glview->setDesignResolutionSize(1024, 768, ResolutionPolicy::NO_BORDER);

	auto frameSize = glview->getFrameSize();
	
	vector<string> searchPath;
	searchPath.push_back("RasterDBViewResources");
	// set searching path
	FileUtils::getInstance()->setSearchPaths(searchPath);

	// turn on display FPS
	director->setDisplayStats(true);

	// set FPS. the default value is 1.0/60 if you don't call this
	director->setAnimationInterval(1.0 / 60);

	screen = make_shared<Screen>();
	screen->init();
	screenModel = make_shared<ScreenModel>();
	screenModel->setScreen(screen);

	director->runWithScene(screen->getScene());
	return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
	Director::getInstance()->stopAnimation();

	// if you use SimpleAudioEngine, it must be pause
	// SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
	Director::getInstance()->startAnimation();

	// if you use SimpleAudioEngine, it must resume here
	// SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
}
