#include "stdafx.h"
#include "ScreenModel.h"
#include "Widget.h"
#include "WidgetGroup.h"
#include "MapWidget.h"

USING_NS_CC;

void ScreenModel::setScreen(shared_ptr<Screen> screen) {
	this->screen = screen;
	MapWidget w;
	w.update();
}


void ScreenModel::add(WidgetGroup group) {
	widgetGroups.push_back(group);

	auto widgets = group.getWidgets();

	for (auto& widget: widgets) {
		screen->getScene()->addChild(widget.getLayer());
	}
}


void ScreenModel::add(Widget widget, const string& groupName) {
	bool success;

	for (auto& group: widgetGroups) {

		if (group.getName() == groupName) {
			group.addWidget(widget);
			success = true;
			break;
		}
	}

	if (!success) {
		throw logic_error("���������� �������: ������ " + groupName + " �� �������");
	}
}