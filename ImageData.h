#pragma once
#include "MapData.h"

class ImageData: public MapData {
public:
	virtual ~ImageData();
	virtual const GeoPt topLeft() const = 0;
	virtual const GeoPt bottomRight() const = 0;
	virtual const double resolution() const = 0;
	virtual const PointD toPlanar(const GeoPt& geoPoint) const = 0;

	virtual unsigned char* scanLine(const char* line, const int left, const int width) const = 0;
};

