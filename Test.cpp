#include "stdafx.h"
#include "Test.h"
#include "gtest\gtest.h"
#include "windows.h"
#include <locale>

void test::run() {

	if (AllocConsole()) {
		freopen("CONIN$", "r", stdin);
		freopen("CONOUT$", "w", stdout);
		freopen("CONOUT$", "w", stderr);

		SetConsoleTitle("GTest console");
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED);
		setlocale(LC_ALL, "Russian");
	}
	int argc = 1;
	char* args[] = { "pn" };

	testing::InitGoogleTest(&argc, args);
	RUN_ALL_TESTS();

	std::cin.get();
	FreeConsole();

	std::wcout.clear();
	std::cout.clear();
	std::wcerr.clear();
	std::cerr.clear();
	std::wcin.clear();
	std::cin.clear();
}