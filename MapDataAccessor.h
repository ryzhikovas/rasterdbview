#pragma once
#include "stdafx.h"
class MapData;

class MapDataAccessor {
public:
	virtual ~MapDataAccessor();
	virtual vector<shared_ptr<MapData>> get(const GeoPt& topLeft, const GeoPt& bottomRight, const double res) const = 0;
};

