#pragma once
#include "stdafx.h"
class Screen;
class ScreenModel;
class MercatorMap;

class AppDelegate : private cocos2d::Application {
	shared_ptr<Screen> screen;
	shared_ptr<ScreenModel> screenModel;
	shared_ptr<MercatorMap> map;
public:
	AppDelegate();
	virtual ~AppDelegate();

	virtual void initGLContextAttrs();

	/**
	@brief    Implement Director and Scene init code here.
	@return true    Initialize success, app continue.
	@return false   Initialize failed, app terminate.
	*/
	virtual bool applicationDidFinishLaunching();

	/**
	@brief  The function be called when the application enter background
	@param  the pointer of the application
	*/
	virtual void applicationDidEnterBackground();

	/**
	@brief  The function be called when the application enter foreground
	@param  the pointer of the application
	*/
	virtual void applicationWillEnterForeground();
};