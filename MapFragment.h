#pragma once
#include "GeoPoint.h"

class MapFragment {
private:
	GeoPt mid;
	SizeD latLonSize;
	double res;
public:
	MapFragment() = delete;
	static MapFragment build(const GeoPt& mid, const SizeD& latLonSize, const double resolution);

	MapFragment& setPos(const GeoPt& mid);
	MapFragment& setSize(const SizeD& size);
	MapFragment& setResolution(const double resolution);

	const GeoPt topLeft() const;
	const GeoPt bottomRight() const;
	const double resolution() const;
private:
	MapFragment(const GeoPt& mid, const SizeD& latLonSize, const double resolution);
};

