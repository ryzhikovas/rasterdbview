#pragma once


template <class T>
struct GeoPointNative {
	GeoPointNative(T x, T y) : x(x), y(y) {}
	T x, y;
};


template <class T, unsigned long xBorder, unsigned long yBorder>
class GeoPoint {
private:
	GeoPointNative<T> p;
public:
	inline GeoPoint();
	inline GeoPoint(const T x, const T y);
	inline GeoPoint(const NativePoint<T>& point);

	inline GeoPointNative<T> native() const;

	inline const T x() const;
	inline const T y() const;

	inline GeoPoint& set(const T x, const T y);
	inline GeoPoint& setX(const T x);
	inline GeoPoint& setY(const T y);

	inline GeoPoint& moveX(const T dx);
	inline GeoPoint& moveY(const T dy);

	template <class T_, unsigned long xBorder_, unsigned long yBorder_>
	friend inline const GeoPoint<T_, xBorder_, yBorder_> operator+(
			const GeoPoint<T_, xBorder_, yBorder_>& left, const GeoPoint<T_, xBorder_, yBorder_>& right);

	template <class T_, unsigned long xBorder_, unsigned long yBorder_>
	friend inline const GeoPoint<T_, xBorder_, yBorder_> operator-(
		const GeoPoint<T_, xBorder_, yBorder_>& left, const GeoPoint<T_, xBorder_, yBorder_>& right);
private:
	static inline const T plusLongitude(T value, unsigned long valueBorder, T adden);
	static inline const T plusLatitude(T value, unsigned long valueBorder, T adden);
};


using GeoPt = GeoPoint<double, 180, 90>;


template <class T, unsigned long xBorder, unsigned long yBorder>
GeoPoint<T, xBorder, yBorder>::GeoPoint():
	p(0, 0) {
}


template <class T, unsigned long xBorder, unsigned long yBorder>
GeoPoint<T, xBorder, yBorder>::GeoPoint(const T x, const T y):
	p(x, y) {
}


template <class T, unsigned long xBorder, unsigned long yBorder>
GeoPoint<T, xBorder, yBorder>::GeoPoint(const NativePoint<T>& point):
	p(point) {
}

template <class T, unsigned long xBorder, unsigned long yBorder>
GeoPointNative<T> GeoPoint<T, xBorder, yBorder>::native() const {
	return p;
}


template <class T, unsigned long xBorder, unsigned long yBorder>
const T GeoPoint<T, xBorder, yBorder>::x() const {
	return p.x;
}


template <class T, unsigned long xBorder, unsigned long yBorder>
const T GeoPoint<T, xBorder, yBorder>::y() const {
	return p.y;
}


template <class T, unsigned long xBorder, unsigned long yBorder>
GeoPoint<T, xBorder, yBorder>& GeoPoint<T, xBorder, yBorder>::set(const T x, const T y) {
	return setX(x).setY(y);
}


template <class T, unsigned long xBorder, unsigned long yBorder>
GeoPoint<T, xBorder, yBorder>& GeoPoint<T, xBorder, yBorder>::setX(const T x) {
	p.x = x;
	return *this;
}


template <class T, unsigned long xBorder, unsigned long yBorder>
GeoPoint<T, xBorder, yBorder>& GeoPoint<T, xBorder, yBorder>::setY(const T y) {
	p.y = y;
	return *this;
}


template <class T, unsigned long xBorder, unsigned long yBorder>
const T GeoPoint<T, xBorder, yBorder>::plusLongitude(T value, unsigned long valueBorder, T adden) {
	auto offseted = value + valueBorder;
	offseted += adden;

	T rest;
	T offsettedTrunc;
	rest = std::modf(offseted, &offsettedTrunc);
	long offsettedTruncInt = static_cast<long>(offsettedTrunc);
	offsettedTruncInt = offsettedTruncInt % static_cast<long>((2 * valueBorder));

	offseted = offsettedTruncInt + rest;

	if (offseted <= 0) {
		offseted += static_cast<T>(valueBorder);
	} else {
		offseted -= static_cast<T>(valueBorder);
	}
	return offseted;
}


template <class T, unsigned long xBorder, unsigned long yBorder>
const T GeoPoint<T, xBorder, yBorder>::plusLatitude(T value, unsigned long valueBorder, T adden) {
	const auto offsetted = value + valueBorder + adden;

	auto offsettedTrunc = trunc(offsetted);
	long offsettedTruncInt = static_cast<long>(offsettedTrunc);
	T rest = offsetted - offsettedTrunc;

	const long signedValueBorder = static_cast<long>(valueBorder);
	const long twoValueBorder = 2 * signedValueBorder;

	if ((offsettedTruncInt / twoValueBorder) % 2) {
		return signedValueBorder - (offsettedTruncInt % twoValueBorder + rest);
	} else {
		return (offsettedTruncInt % twoValueBorder + rest) - signedValueBorder;
	}
}


template <class T, unsigned long xBorder, unsigned long yBorder>
GeoPoint<T, xBorder, yBorder>& GeoPoint<T, xBorder, yBorder>::moveX(const T dx) {
	return this->setX(GeoPoint<T, xBorder, yBorder>::plusLongitude(x(), xBorder, dx));
}


template <class T, unsigned long xBorder, unsigned long yBorder>
GeoPoint<T, xBorder, yBorder>& GeoPoint<T, xBorder, yBorder>::moveY(const T dy) {
	 return this->setY(GeoPoint<T, xBorder, yBorder>::plusLatitude(y(), yBorder, dy));
}


template <class T, unsigned long xBorder, unsigned long yBorder>
const GeoPoint<T, xBorder, yBorder> operator+(
	const GeoPoint<T, xBorder, yBorder>& left, const GeoPoint<T, xBorder, yBorder>& right) {
	GeoPoint<T, xBorder, yBorder> point;

	point.setX(GeoPoint<T, xBorder, yBorder>::plusLongitude(
		left.x(), xBorder, right.x())
	);

	point.setY(GeoPoint<T, xBorder, yBorder>::plusLatitude(
		left.y(), yBorder, right.y())
	);
	return point;
}


template <class T, unsigned long xBorder, unsigned long yBorder>
const GeoPoint<T, xBorder, yBorder> operator-(
	const GeoPoint<T, xBorder, yBorder>& left, const GeoPoint<T, xBorder, yBorder>& right) {
	GeoPoint<T, xBorder, yBorder> point;

	point.setX(GeoPoint<T, xBorder, yBorder>::plusLongitude(
		left.x(), xBorder, -right.x())
		);

	point.setY(GeoPoint<T, xBorder, yBorder>::plusLatitude(
		left.y(), yBorder, -right.y())
		);
	return point;
}