#pragma once
#include "Map.h"
#include "MercatorPoint.h"

class MercatorMap: public Map {
private:
	MercPt screenMid;
	SizeI size;
public:
	MercatorMap() = delete;
	static shared_ptr<MercatorMap> build(const SizeI& screenSize, const MercPt& geoPos =
			MercPt(GeoPt(37.621485, 55.747154)));

	const MapFragment currentFragment() const override;
	const MercPt topLeftScreen() const;
	const MercPt bottomRightScreen() const;

	const SizeI screenSize() const override;
	void resizeScreen(const SizeI& size) override;

	const GeoPt screenToGeo(const PointD& screenPoint) const override;
	const PointD geoToScreen(const GeoPt& geo) const override;

	void toPoint(const GeoPt& point) override;
	void zoomUp(const unsigned modifer);
	void zoomDown(const unsigned modifer);
private:
	MercatorMap(const MercPt& screenMid, const SizeI& size);
};

