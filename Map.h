#pragma once
#include "stdafx.h"
#include "MapFragment.h"

class Map {
public:
	virtual ~Map();
	virtual const MapFragment currentFragment() const = 0;
	virtual void resizeScreen(const SizeI& size) = 0;
	virtual const SizeI screenSize() const = 0;

	virtual const GeoPt screenToGeo(const PointD& screenPoint) const = 0;
	virtual const PointD geoToScreen(const GeoPt& geo) const = 0;
	
	virtual void toPoint(const GeoPt& point) = 0;
};

