#pragma once
#include <gtest/gtest.h>
#include "MercatorPoint.h"

namespace test {
	class MercatorPointTest : public ::testing::Test {
	protected:
		MercPt p1;
	protected:
		void SetUp() override;
		void assertPoint(const MercPt& p1, const MercPt& p2) const;
	};
}


