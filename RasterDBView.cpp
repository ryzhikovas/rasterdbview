#include "stdafx.h"
#include "AppDelegate.h"
#include "Test.h"

int main(int argc, char *argv[]) {

	if (argc > 1 && string(argv[1]) == "test") {
		test::run();
	} else {
		shared_ptr<AppDelegate> rasterDBView;
		rasterDBView = make_shared<AppDelegate>();
		cocos2d::Application::getInstance()->run();
	}
	return 0;
}
