#pragma once
#include "stdafx.h"
#include "Widget.h"

class WidgetGroup {
private:
	vector<Widget> widgets;
	string name;
public:
	void addWidget(Widget widget);
	string getName() const;
	void setName(const string& name);

	vector<Widget> getWidgets();
};

