#include "stdafx.h"
#include "WidgetGroup.h"

void WidgetGroup::addWidget(Widget widget) {
	widgets.push_back(widget);
}


string WidgetGroup::getName() const {
	return name;
}

void WidgetGroup::setName(const string& name) {
	this->name = name;
}

vector<Widget> WidgetGroup::getWidgets()  {
	return widgets;
}