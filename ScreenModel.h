#pragma once
#include "stdafx.h"
#include "WidgetGroup.h"
#include "Screen.h"


class ScreenModel {
private:
	shared_ptr<Screen> screen;
	vector<WidgetGroup> widgetGroups;
public:
	void setScreen(shared_ptr<Screen> screen);

	void add(WidgetGroup group);
	void add(Widget widget, const string& groupName);
};

