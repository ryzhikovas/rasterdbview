#include "stdafx.h"
#include "MercatorPointTest.h"

namespace test {
	TEST_F(MercatorPointTest, PointMoveTest) {
		p1.moveX(90.0).moveY(20.0);
		assertPoint(p1, MercPt(-38.0, 20.0));
	}


	TEST_F(MercatorPointTest, BaseTest) {
		assertPoint(p1, MercPt(128.0, 0.0));
	}


	TEST_F(MercatorPointTest, ConvertTest) {
		const auto geoPoint = MercPt().geo();
		ASSERT_DOUBLE_EQ(geoPoint.x(), 0.0);
		ASSERT_DOUBLE_EQ(geoPoint.y(), 0.0);

		const auto gp = GeoPt(-50, -20);
		MercPt mercatorPoint(gp, 256 * 13);
		ASSERT_DOUBLE_EQ(mercatorPoint.geo().x(), gp.x());
		ASSERT_NEAR(mercatorPoint.geo().y(), gp.y(), 0.0000001);
	}


	void MercatorPointTest::SetUp() {
		p1 = MercPt(GeoPt(180, 0));
	}


	void MercatorPointTest::assertPoint(const MercPt& p1, const MercPt&p2) const {
		ASSERT_DOUBLE_EQ(p1.x(), p2.x());
		ASSERT_NEAR(p1.y(), p2.y(), 0.0000001);
	}
}
