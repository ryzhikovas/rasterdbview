#include "stdafx.h"
#include <string>
#include <chrono>
#include "GeoPointTest.h"

namespace test {
	TEST_F(GeoPointTest, PointsSumTest) {
		const auto t1 = p1 + GP(-175.0, -80.0);
		assertPoint(t1, GP(175.0, -20.0));

		const auto t2 = p1 + GP(370.0, 80.0);
		assertPoint(t2, GP(0.0, 40.0));

		const auto t3 = p1 + GP(0.001, -0.001);
		assertPoint(t3, GP(-9.999, 59.999));

		const auto t4 = p1 + GP(0.7001, -0.7001);
		assertPoint(t4, GP(-9.2999, 59.2999));
	}


	TEST_F(GeoPointTest, PointMoveTest) {
		auto p = p1;
		assertPoint(p.moveX(-175.0), GP(175.0, 60.0));
		assertPoint(p.moveY(-80.0), GP(175.0, -20.0));

		p = p1;
		assertPoint(p.moveX(0.001).moveY(-0.001), GP(-9.999, 59.999));
	}


	void GeoPointTest::SetUp() {
		p1.set(-10.0, 60.0);
	}


	void GeoPointTest::assertPoint(const GP& p1, const GP& p2) const {
		ASSERT_FLOAT_EQ(p1.x(), p2.x());
		ASSERT_FLOAT_EQ(p1.y(), p2.y());
	}
}
