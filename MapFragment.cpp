#include "stdafx.h"
#include "MapFragment.h"


MapFragment::MapFragment(const GeoPt& mid, const SizeD& latLonSize, const double resolution) :
	mid(mid),
	latLonSize(latLonSize),
	res(resolution) {
}


MapFragment MapFragment::build(const GeoPt& mid, const SizeD& latLonSize, const double resolution) {

	if (latLonSize.width() <= 0 || latLonSize.height() <= 0) {
		throw logic_error("������� �������� ������� ��������� �����");
	}
	return MapFragment(mid, latLonSize, resolution);
}


MapFragment& MapFragment::setPos(const GeoPt& mid) {
	this->mid = mid;
	return *this;
}


MapFragment& MapFragment::setSize(const SizeD& size) {
	this->latLonSize = size;
	return *this;
}


MapFragment& MapFragment::setResolution(const double resolution) {
	this->res = resolution;
	return *this;
}


const GeoPt MapFragment::topLeft() const {
	GeoPt tl(mid);
	tl.moveX(-latLonSize.width()).moveY(latLonSize.height());
	return tl;
}


const GeoPt MapFragment::bottomRight() const {
	GeoPt br(mid);
	br.moveX(latLonSize.width()).moveY(-latLonSize.height());
	return br;
}


const double MapFragment::resolution() const {
	return res;
}
