#include "stdafx.h"
#include "MapWidget.h"
#include "Map.h"
#include "MapData.h"
#include "GeoPoint.h"

using uchar = unsigned char;

MapWidget::~MapWidget() {
}


void MapWidget::update() const {
	const auto fragment = sceneMap->currentFragment();
	const auto data = accessor->get(fragment.topLeft(), fragment.bottomRight(), fragment.resolution());

	for_each(data.begin(), data.end(), [this] (const shared_ptr<MapData> data) {
		auto sprite = fromData(data);
		layer->addChild(sprite);

		const auto screenTopLeft = sceneMap->geoToScreen(data->topLeft());
		sprite->setPosition(screenTopLeft.x(), screenTopLeft.y());
	});
}


cocos2d::Sprite* MapWidget::fromData(const shared_ptr<MapData>& data) const {
	const auto screenTopLeft = sceneMap->geoToScreen(data->topLeft());
	const auto screenBottomRight = sceneMap->geoToScreen(data->bottomRight());

	SizeI dataSize(screenBottomRight.x() - screenTopLeft.x(), screenBottomRight.y() - screenTopLeft.y());

	vector<uchar> buffer;
	buffer.reserve(dataSize.height() * dataSize.width());
	GeoPt geo;

	for (int i = 0; i < dataSize.height(); ++i) {

		for (int j = 0; j < dataSize.width(); ++j) {
			geo = sceneMap->screenToGeo(screenTopLeft + PointD(j, i));
			buffer.push_back(data->pixel(data->toPlanar(geo)));
		}
	}

	auto texture = new cocos2d::Texture2D();
	texture->autorelease();
	texture->initWithData(&buffer.front(), buffer.size(),
		cocos2d::Texture2D::PixelFormat::I8,
		dataSize.width(), dataSize.height(),
		cocos2d::Size(dataSize.width(), dataSize.height()));

	cocos2d::Sprite* sprite = cocos2d::Sprite::create();
	sprite->initWithTexture(texture);
	return sprite;
}
