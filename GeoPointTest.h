#pragma once
#include <gtest/gtest.h>
#include "GeoPoint.h"

namespace test {
	class GeoPointTest : public ::testing::Test {
	protected:
		using GP = GeoPt;
		GP p1;
	protected:
		void SetUp() override;
		void assertPoint(const GP& p1, const GP&p2) const;
	};
}

