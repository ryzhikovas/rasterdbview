#pragma once
#include "stdafx.h"
#include "GeoPoint.h"


class MapData {
public:
	virtual ~MapData();

	virtual const GeoPt topLeft() const = 0;
	virtual const GeoPt bottomRight() const = 0;
	virtual const double resolution() const = 0;
	virtual const PointD toPlanar(const GeoPt& geoPoint) const = 0;
	virtual const uchar pixel(const PointD& point) const = 0;
};

